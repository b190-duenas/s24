let getCube = 2**3;
console.log(`The cube of 2 is ${getCube}`);

let address = ["258 Washington Ave NW","California 90011"];
let [address1,address2] = address;
console.log(`I live at ${address1}, ${address2}`);

let animal = {
	name: "Lolong",
	breed: "saltwater crocodile",
	weight: 1075,
	measurement: "20 ft 3 in"
}

let {animalName,animalBreed,animalWeight,animalMeasurement} = animal;
console.log(`${animalName} was a ${animalBreed}. He weighed at ${animalWeight} kgs with a measurement of ${animalMeasurement}`);

let numArr = [1,2,3,4,5];
numArr.forEach(num => console.log(`${num}`))


const initial = 0
const sum = numArr.reduce(
	(previous,current) => previous + current, initial);
console.log(sum);

class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog =new Dog("Dexter",4,"Labrador");
console.log(myDog);